import pandas

if __name__ == "__main__":
    inputtbl = "/media/vincentius/Martin/bio/simple_somatic_mutation.open.tsv"
    rowidx = 7207367 
    # idx 0 is the header

    c = pandas.read_csv(inputtbl,
                        sep='\t',
                        usecols=['chromosome','chromosome_start','mutation_type','reference_genome_allele','mutated_from_allele','mutated_to_allele'],
                        skiprows=range(1,rowidx+1),
                        nrows=1)
    print(c)
