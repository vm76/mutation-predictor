import pandas as pd
import time
import os
import pickle
import sys

def read_tsv(filepath,todict=True):
    tsv = pd.read_csv(filepath, 
                    sep='\t',
                    # 'icgc_mutation_id','chromosome_end'
                    usecols=['chromosome','chromosome_start','mutation_type','reference_genome_allele','mutated_from_allele','mutated_to_allele'])
    tsv = tsv[tsv['mutation_type'].apply(lambda x: "single base substitution" == x)].drop('mutation_type',1).drop_duplicates() # only take single base mutation     
    #print(tsv['chromosome'].unique())    
    if todict:
        print("Unique single base-pair mutations found: {}".format(tsv.shape[0]))
        #tsv['chromosome'] = tsv['chromosome'].astype(int)  # convert to int so it can be sorted  
        grouped = tsv.groupby('chromosome',sort=True)
        gdict = {key:item for key,item in grouped}
        return gdict #reset_index(drop=True) #grouped.get_group(key)
    else:
        return tsv

if __name__ == '__main__':
    #path = '/media/vincentius/Martin/bio/simple_somatic_mutation.open.tsv'
    path=sys.argv[1]

    tsv = read_tsv(path)
    tsvname = os.path.splitext(os.path.basename(path))[0]    

    start = time.time()
    with open("{}-unique.pickle".format(tsvname), 'wb') as f:
        pickle.dump(tsv, f) #pickle.load(f)
    print("Total time to read the tsv file {:.5f}secs".format(time.time()-start))
