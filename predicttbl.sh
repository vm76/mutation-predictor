#!/bin/bash

cp /dev/null pred_result.txt

RES=$(sbatch readtbl.py) && sbatch --dependency=afterok:${RES##* } postprocess.py
