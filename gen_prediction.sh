#!/bin/bash  

#SBATCH -o vm_%A_%a.out
#SBATCH -e vm_%A_%a.err

#SBATCH --array=0-114
#SBATCH --mem=20G
#SBATCH --mail-type=END
#SBATCH --mail-user=vm76@duke.edu

# Directory setting
output="/gpfs/fs0/data/gordanlab/vincentius"
input=(/home/vm76/tf/*)
# Parameter configuration
kmer=4
chunk=1
#------------End of Configuration-----------------

#echo ${#input[@]}

filein=${input[SLURM_ARRAY_TASK_ID]}
echo $filein
echo "task_id: $SLURM_ARRAY_TASK_ID"

python olskmer.py $filein $output -k $kmer -d $chunk



