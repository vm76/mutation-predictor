# A script to read prediction, chromosome data, and the input mutation. It calculates
# the diff and the t-value.
# This script calculates only SINGLE diff and t-value wtihout reading the table

import pandas as pd
import numpy as np
import sys,os
import pickle
import bio
import time
import csv,gzip
from operator import itemgetter
from collections import defaultdict

def print_full(x):
    y = pd.DataFrame(x)
    pd.set_option('display.max_rows', len(y))
    print(y)
    pd.reset_option('display.max_rows')


def seq_diff(wild,mut,k):
    if len(wild) != len(mut):
        print("Input error!")
        return 0
    total = defaultdict(int)
    for i in range(len(mut)-(k-1)):
        r1 = bio.seqtoi(mut[i:i+k])
        rc1 = bio.revcomp(r1)
        if r1 > rc1:
            r1 = rc1
        total[r1] += 1
        
        r2 = bio.seqtoi(wild[i:i+k])
        rc2 = bio.revcomp(r2)
        if r2 > rc2:
            r2 = rc2
        total[r2] -= 1
    return total

def count_seq(seq):
    if(seq[-1] == seq[int(len(seq)/2-1)]): # no mutation
        print("sequence with no mutation detected :" + seq)
        return 0
    if len(seq) % 2 != 0:
        print("Must be an even number")
        return 0
    k = int(len(seq) / 2)
    orig_context =  seq[int(len(seq)/2-1)]
    mut_context =  seq[-1]
    orig_seq = seq[:-1]
    mut_seq = orig_seq[:(kmer-1)] + mut_context + orig_seq[kmer:]

    diffcount = seq_diff(orig_seq,mut_seq,k)
    return diffcount

def diff_count(count,params):
    diffbeta = 0
    for key,value in count.items():
        diffbeta += value * params.at[key,'param']
    return diffbeta

def sd_diff(count,var):
    total = 0
           
    for key in count:
        #print(key)
        tmp = 0
        for olig in count:
            tmp += var.ix[key,olig] * count[olig]
        total += count[key] * tmp
    
    return np.sqrt(total)

def get_chrom(cfile):
    chrom = str()   
    with open(cfile) as fa:
        next(fa) #skip the header
        chrom = fa.read().replace('\n', '')
    return chrom

def get_seq(cpos,mutatedto,cfile):
    data,chrom = str(),str()   
    with gzip.open(cfile,'rb') as f:
        chrom = next(f)
        data = f.read().decode('utf-8').replace('\n','')
    #rowidx = 36
    pos = cpos - 1
    return data[pos-5:pos+6] + mutatedto
    #print(df.loc[df['dna_seq'] == (data[pos-5:pos+6] + tsv.at[rowidx,'mutated_to_allele'])])

def diff_t(count,predtbl): #predtbl: beta and sigma
    if count == 0: #no mutation case
        return 0,0
    #CACCTTGGCCTC  0.31749  8.50134
    diff = diff_count(count,predtbl)
    sd = sd_diff(count,predtbl) #predtbl.drop('param',axis=1)
    t = diff/sd
    return diff,t

def get_ols_res(in_dir):
    res = dict()
    ols_files = list()
    for fname in os.listdir(in_dir):
        if fname.startswith("ols6mer"): #hardcoded for now
            ols_files.append(fname) #TODO: might need to adjust here
            with open(in_dir + "/" + fname, 'rb') as f:
                res[fname] = pickle.load(f)
    return sorted(ols_files),res

def read_tsv(filepath,todict=True):
    tsv = pd.read_csv(filepath, 
                    sep='\t',
                    # 'icgc_mutation_id','chromosome_end'
                    usecols=['chromosome','chromosome_start','mutation_type','reference_genome_allele','mutated_from_allele','mutated_to_allele'])
    tsv = tsv[tsv['mutation_type'].apply(lambda x: "single base substitution" == x)].drop('mutation_type',1).drop_duplicates() # only take single base mutation     
    #print(tsv['chromosome'].unique())    
    if todict:
        #tsv['chromosome'] = tsv['chromosome'].astype(int)  # convert to int so it can be sorted  
        grouped = tsv.groupby('chromosome',sort=True)
        gdict = {key:item for key,item in grouped}
        return gdict #reset_index(drop=True) #grouped.get_group(key)
    else:
        return tsv

def max_t(l):
    m = max(enumerate(l),key=itemgetter(1))
    return [m[0]]+list(m[1])

def predict(tsv, ols_files,models,action):
    totalchroms = 22
    predres = [0,0,0]
    rowres = 0
    totalrow = 0
    counter = 0
    n = 1000

    for k,v in tsv.items():
        totalrow += v.shape[0]

    start_time = time.time()
    for cidx in [str(a) for a in range(1,23)] + ['X','Y']:
        if n != 0 and counter >= n:
            break
        if cidx not in tsv:
            continue
        #print(tsv[cidx])
        print("Chromosome: " + str(cidx))
        cfile = cpath + str(cidx) + '.fa'
        chromosome = get_chrom(cfile)
        for idx,row in tsv[cidx].iterrows():
            if counter % 100 == 0:
                print("Progress: {}/{} in {:.2f}secs".format(counter,totalrow,time.time()-start_time))
            counter += 1
            if n!= 0 and counter >= n:
                break
            #start_time = time.time()
            pos = row['chromosome_start'] - 1
            if row['mutated_from_allele'] != chromosome[pos]:
                print("Found mismatch in the mutation!")
                continue
            seq = chromosome[pos-5:pos+6] + row['mutated_to_allele']
            count = count_seq(seq) 
            res = list()
            for tf in ols_files:
                res.append(diff_t(count,models[tf])) #0.01 secs for each computation
            if action == 'maxt':            
                mt = max_t(res) 
                if mt[2] > predres[2]:
                    predres = mt
                    predres.append(idx)
                    rowres = row
                    #print(predres) # currently #tf,diff,t,#tbl
            #print(pd.DataFrame(res,columns=['diff','t']))
            #print("Time taken for this row: {:.2f}secs".format(time.time() - start_time))
    return predres,rowres

def process(tsv,cpath,pred_dir): # tsv file as input, chromosome path, directory with rpediction
    print("Loading all predictions...") 
    start_time = time.time()   
    ols_files,models = get_ols_res(pred_dir)
    print("Finished loading models, time taken: {:.2f}secs".format(time.time() - start_time))
    #predres = dict()
    #total = tsv.index[-1]
    res = predict(tsv, ols_files, models,action='maxt')
    return res

def check():  
    predictpath = 'ols6mer-sample.pickle'
    orig = 'olskmer-result.csv'
    tsv = read_tsv('/home/vincentius/Research/data/simple_somatic_mutation.tsv',todict=False) #/media/vincentius/Martin/bio/simple_somatic_mutation.open.tsv
    orig_res = pd.read_csv(orig,sep=' ')
    start = time.time()
    with open(orig, mode='r') as infile:
        reader = csv.reader(infile,delimiter=' ')
        orig_dict = {el[0]: el[1:3]  for el in reader}
    print("Time to convert to dict (for 1 TF): {:.3f}sec".format(time.time()-start))
    with open(predictpath, 'rb') as f:
        ols_res = pickle.load(f)
    for i,row in tsv.iterrows():
        chromosome = cpath + tsv.at[i,'chromosome'] + '.fa.gz'
        seq = get_seq(tsv.at[i,'chromosome_start'],tsv.at[i,'mutated_to_allele'],chromosome)
        print(seq)
        start = time.time()
        predres = diff_t(count_seq(seq),ols_res)
        print("Result from readtbl prediction: {:.5f}, {:.5f} in {:.8f}sec".format(predres[0],predres[1],time.time()-start))
        start = time.time()
        origres = orig_res.loc[orig_res['dna_seq'] == seq]
        print("From table lookup: {} in {:.8f}sec".format(origres.to_string(index=False,header=False),time.time()-start))
        start = time.time()
        print("With dictionary lookup: {} in {:.8f}sec".format(orig_dict[seq],time.time()-start))
        print("---------------------------------")

if __name__ == '__main__':
    kmer = 6    
    pred_dir = '/home/vincentius/Research/olskmer/prediction'
    cpath = '/home/vincentius/Research/data/Homo_sapiens.GRCh37.75.dna.chromosome.'

    #/home/vincentius/Research/data/simple_somatic_mutation.tsv /media/vincentius/Martin/bio/simple_somatic_mutation.open.tsv
    s = time.time()
    tsv = read_tsv('/home/vincentius/Research/data/simple_somatic_mutation.tsv') #3,464,501 total in real data
    print("Time to read tsv file {:.2f}secs".format(time.time()-s))
    #print("Last index from tsv: {}".format(tsv.index[-1]))
    check()
    
    #pred = process(tsv,cpath,pred_dir)
    #print(pred)
    
    #print([key for key in pred])

