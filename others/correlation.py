import time
import os,sys
import pandas as pd
import numpy as np
import statsmodels.formula.api as sm
from scipy.stats.stats import pearsonr
from collections import defaultdict

import bio

def adjustscr(score,shift=1000):
    minscr = score[score.idxmin()]
    score_frame = pd.DataFrame(np.log(score - minscr + shift),columns=['score'])
    return score_frame

def read_pbm(filename,kmer):
    tbl = pd.read_csv(filename,names=['score','sequence'],delim_whitespace=True) #score,sequence ..
    score = adjustscr(tbl['score'],1000)
    seqbin = [bio.seqtoi(x) for x in tbl['sequence']]
    oligfreq = bio.nonr_olig_freq(seqbin,kmer) #tbl['sequence'].tolist()
    return pd.concat([score,oligfreq],axis=1)

def parse_dir(source):
    if not os.path.exists(source) or os.listdir(source) == []:
        print("Found empty or non-existing directory!")
        sys.exit(0)
    data_in = defaultdict(dict)
    for root, dirnames, filenames in os.walk(source):
        for filename in filenames:
            name,extension = filename.split(".",1)
            if filename.endswith("v1_deBruijn.txt"): 
                idx = filename.index("v1_deBruijn")   
                data_in[filename[:idx]]['train'] = os.path.join(root,filename)
            elif filename.endswith("v2_deBruijn.txt"):
                idx = filename.index("v2_deBruijn")
                data_in[filename[:idx]]['test'] = os.path.join(root,filename)
    for key in data_in:
        #print(data_in[key]['test'] + " " + data_in[key]['train'])
        if len(data_in[key]) < 2:
            print("Found data with no train/test: " + key)
            del data_in[key]
    return data_in

#def plot_pearson(rval):
#    train_all = list()
#    test_all = list()
#    for kmer in rval:
#        train_all.append(rval[kmer]['train'])
#        test_all.append(rval[kmer]['test'])
#    plt.boxplot(train_all)
#    plt.savefig("train_all.png")
#    plt.close()
#    plt.boxplot(test_all)
#    plt.savefig("test_all.png")
#    plt.close()

if __name__ == "__main__": 
    kmer_list = [6]
    div = 1
    in_dir = "In" 

    start_time = time.time()
    inputs = parse_dir(in_dir)
    for kmer in kmer_list:
        idx = 0
        bio.gen_noreversed_kmer(kmer) 
        f = open('prediction-{}mer.dat'.format(kmer),'w')
        f.write("dataname rtrain rtest time(secs)\n")
        print("K-value: " + str(kmer))
        for key in sorted(inputs.keys()):
            data = inputs[key]
            start_tf = time.time()
            idx += 1
            print("======Progress {}/{} for {}mer : {}======".format(idx,len(inputs),kmer,key))
            rtrain,rtest = 0,0
            df_train = read_pbm(data['train'],kmer)
            lm = sm.OLS(df_train['score'],df_train.drop('score',axis=1)).fit()
            y_train = lm.predict(df_train.drop('score',axis=1))
            rtrain = pearsonr(y_train,df_train['score'])
            
            if data['test']:
                df_test = read_pbm(data['test'],kmer)
                y_test = lm.predict(df_test.drop('score',axis=1))
                rtest = pearsonr(y_test,df_test['score'])

            f.write('{} {:.6f} {:.6f} {:4f}\n'.format(key[:-1],rtrain[0],rtest[0],(time.time() - start_tf)))
    
            print('rtrain: {:.4f}, rtest: {:.4f}, time: {:.4f}secs'.format(rtrain[0],rtest[0],(time.time() - start_tf)))
        f.close()
        
    print("Total time: {:.2f}seconds".format(time.time() - start_time))
    print("Done!")
