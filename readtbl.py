#!/bin/env python

#SBATCH -o vm_%A_%a.out
#SBATCH -e vm_%A_%a.err

#SBATCH --array=0-114%115
#SBATCH --mem=20G
#SBATCH --mail-type=END
#SBATCH --mail-user=vm76@duke.edu

import sys,os
import pandas as pd
import pickle
import time
import gzip

# ------ CONFIGURATION PART ----------
# chromosome path:
cpath = '/home/vincentius/Research/data/Homo_sapiens.GRCh37.75.dna.chromosome.'
# dataset generated from data reader
dataset = "simple_somatic_mutation.open-unique.pickle"
# predictions directory
indir = "/gpfs/fs0/data/gordanlab/vincentius/"
# ------ END OF PART ----------

nucleotides = {'A':0,'C':1,'G':2,'T':3}

def get_chrom(cfile):
    with gzip.open(cfile,'rb') as f:
        next(f)
        chrom = f.read().decode('utf-8').replace('\n','')
    return chrom

def seqtoi(seq):
    rep = 0
    for i in range(0,len(seq)):
        rep <<=2
        rep |= nucleotides[seq[i]]
    #print("seqtoi time {:.8f}".format(time.time()-s))
    return rep

def find_between(s,first,last):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        sys.exit("Prediction must be generated from data reader")
        #return ""

def process(data_in,tf_in):
    kmer = int(find_between(tf_in,"prediction","mer"))

    start = time.time()
    with open(data_in, 'rb') as f:
        dataset = pickle.load(f)
    totalrow = 0
    for k,v in dataset.items():
        totalrow += v.shape[0]
    print("Found {} data for {}mer in {:.5f}secs".format(totalrow,kmer,time.time()-start))
    
    start = time.time()
    #with open(tf_in, 'r') as f:
     #   tf = list(csv.reader(f,delimiter=' '))
    tf = pd.read_csv(tf_in, sep=' ').values.tolist()
    print("Time to get TF: {:.5f}secs".format(time.time()-start))

    i = 0
    step_report = int(totalrow / 50)
    start = time.time()
    tmp_tmax = [-999,-999]
    for cidx in [str(a) for a in range(1,23)] + ['X','Y']:
        print("Iterating dataset for chromosome {}...".format(cidx))
        chromosome = get_chrom(cpath + str(cidx) + '.fa.gz')
        for idx,row in dataset[cidx].iterrows():
            i += 1
            if i % step_report == 0:
                print("{:.1f}% complete, time taken: {:.2f}secs".format((float(i)/totalrow * 100),time.time()-start))
            pos = row['chromosome_start'] - 1
            if row['mutated_from_allele'] != chromosome[pos]:
                sys.exit("Found mismatch in the mutation: \n{}".format(row))
                continue
            seq = chromosome[pos-kmer+1:pos+kmer] + row['mutated_to_allele'] #-5,+6
            #print("sequence: " + seq)          
            diff_t = tf[seqtoi(seq)]
            if(diff_t[1] > tmp_tmax[1]):
                tmp_tmax = diff_t + [seq,idx] #diff,t,sequence,row
    return tmp_tmax
            

if __name__ == '__main__':
    ldir = os.listdir(indir)
    aid = int(os.environ['SLURM_ARRAY_TASK_ID'])

    result = process(dataset,indir+ldir[aid]) + [os.path.basename(ldir[aid])]
    #result = process(dataset,'prediction6mer.Arid3a_3875.1_v1_deBruijn.csv')
    with open("pred_result.txt", "a") as myfile:
        myfile.write(",".join(map(str,result)) + "\n")
    print("End result " + str(result))    

