<html>
<body>

# mutation-predictor

<h1>Generate All Possible Predictions</h1>
<p>
1. Modify all the configurations in the gen_prediction.sh which are input directory, output directory, value of k-mer,
and div (number of memory chunks required). <br />
2. Modify the SLURM configuration, it is located on top of gen_prediction.sh on every line started with #SBATCH <br />
especially the array setting, it should conform to the number of TFs. <br />
3. Run: <code> sbatch gen_prediction.sh </code> <br />
</p>

<h1>To Do Computations -- currently compute sequence with maximum TF</h1>
<p>
1. Run datareader.py against the original table to get the unique single base mutations. <br />
<code>python datareader.py &lt;cancer_data.tsv&gt;</code> <br />
2. Use the result from datareader as "dataset" in readtbl.py. <br />
3. In readtbl.py, set "indir" to the TF data and cpath to the chromosomes path
4. Run using predicttbl.sh <br />
5. See the result in SLURM-&lt;pid&gt; file, it will be in format: <br />
<code>&lt;diff&gt; &lt;t-value&gt; &lt;2k-mer_sequence&gt; &lt;row_number&gt;</code> <br />
6. To check the row in the original file, run: <br />
<code> python getfromtbl.py </code> <br />
Set the <i>inputtbl</i> to the original cancer data file and <i>rowidx</i> to the &lt;row_number&gt; returned from predicttbl
</p>

</body>
</html>
