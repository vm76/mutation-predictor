#!/bin/env python

import csv

if __name__ == "__main__": 
    csvin = "pred_result.txt"

    #print("hello")
    with open(csvin) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        res = list(list(rec) for rec in reader)

    vmax = [0.0,0.0,-999.0]
    for row in res:
        #print(str(row) + "\n")
        if float(row[1]) > float(vmax[1]):
            vmax = row

    print(vmax)
    
